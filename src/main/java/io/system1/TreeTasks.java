package io.system1;

import io.system1.Beans.BTreeNode;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class TreeTasks {
    @Inject
    ExecutorService executorService;

    @Inject
    AtomicLong counter;

    public void printNegatives(BTreeNode node) {
        if (node == null)
            return;

        if (node.getValue() < 0)
            System.out.println(node.getValue());

        printNegatives(node.getLeft());
        printNegatives(node.getRight());
    }

    public void printFirst10NegativesInParallel(BTreeNode node) {
        if (node == null)
            return;

        if (node.getValue() < 0) {
            System.out.println(node.getValue());
            if (counter.incrementAndGet() >= 10) {
                executorService.shutdownNow();
                return;
            }
        }

        executorService.submit(() -> { printFirst10NegativesInParallel(node.getLeft()); });
        executorService.submit(() -> { printFirst10NegativesInParallel(node.getRight()); });
    }
}
