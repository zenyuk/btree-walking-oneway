package io.system1.Beans;

import lombok.Data;

@Data
public class BTreeNode {
    private Long value;
    private BTreeNode left;
    private BTreeNode right;

    public BTreeNode(Long value) {
        this.value = value;
    }

    public BTreeNode(Long value, BTreeNode left, BTreeNode right) {
        this(value);
        this.left = left;
        this.right = right;
    }
}
