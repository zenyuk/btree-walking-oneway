package io.system1;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Component
public class TasksRunner {
    @Inject
    TreeTasks tasks;

    @Inject
    TreeFactory treeFactory;

    @PostConstruct
    public void printNegativeValues() {
        tasks.printFirst10NegativesInParallel(treeFactory.generateTree());
    }
}
