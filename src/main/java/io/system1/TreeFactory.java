package io.system1;

import io.system1.Beans.BTreeNode;
import org.springframework.stereotype.Component;

@Component
public class TreeFactory {
    public BTreeNode generateTree() {

        // level 3
        BTreeNode l3_1 = new BTreeNode(-100l, new BTreeNode(-99l), null);
        BTreeNode l3_2 = new BTreeNode(-101l);
        BTreeNode l3_3 = new BTreeNode(-102l);
        BTreeNode l3_4 = new BTreeNode(-103l);
        BTreeNode l3_5 = new BTreeNode(-104l);
        BTreeNode l3_6 = new BTreeNode(1l);
        BTreeNode l3_7 = new BTreeNode(-105l);
        BTreeNode l3_8 = new BTreeNode(-106l);

        // level 2
        BTreeNode l2_1 = new BTreeNode(-107l, l3_1, l3_2);
        BTreeNode l2_2 = new BTreeNode(1l, l3_3, l3_4);
        BTreeNode l2_3 = new BTreeNode(-108l, l3_5, l3_6);
        BTreeNode l2_4 = new BTreeNode(-109l, l3_7, l3_8);

        // level 1
        BTreeNode l1_1 = new BTreeNode(1l, l2_1, l2_2);
        BTreeNode l1_2 = new BTreeNode(-110l, l2_3, l2_4);

        return new BTreeNode(-111l, l1_1, l1_2);
    }
}
