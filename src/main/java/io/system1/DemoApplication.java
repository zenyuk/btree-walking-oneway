package io.system1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

@SpringBootApplication
public class DemoApplication {

	@Bean
    ExecutorService executorService() {
	    return Executors.newFixedThreadPool(16);
    }

    @Bean
    AtomicLong atomicLong() {
	    return new AtomicLong();
    }

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
